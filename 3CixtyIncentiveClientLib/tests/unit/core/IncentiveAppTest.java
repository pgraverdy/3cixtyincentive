package unit.core;

import static org.junit.Assert.*;

import org.junit.Test;
import org.threecixty.csincentive.core.CSMetadata;
import org.threecixty.csincentive.core.CrowdsourcingManager;
import org.threecixty.csincentive.core.IncentiveDisplayCallback;
import org.threecixty.csincentive.core.IncentiveItem;
import org.threecixty.csincentive.core.IncentiveManager;

public class IncentiveAppTest implements IncentiveDisplayCallback {

	boolean gotIncentive;

	@Test
	public void testCallback() {
		// empty class for now, required for proper init
		CrowdsourcingManager csmgr = new CrowdsourcingManager();
		IncentiveManager mgr = new IncentiveManager(this, csmgr);

		CSMetadata md = new CSMetadata();

		gotIncentive = false;

		mgr.newCrowdsourcingInfo(md);

		// wait a little, then make sure the gotIncentive is true
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
		}
		assertTrue(gotIncentive);
	}

	@Override
	public void displayIncentive(IncentiveItem item) {
		gotIncentive = true;

	}

}
