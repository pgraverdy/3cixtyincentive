package org.threecixty.csincentive.core;

/**
 * Interface to be implemented by the application.
 * 
 * The app must then register the instance of the class implementing this
 * interface with the incentive manager. The incentive manager calls this
 * interface whenever an incetive must be displayed to the user.
 * 
 */
public interface IncentiveDisplayCallback {

	/**
	 * callback from the incentive manager to the application in order to
	 * display an incetive to the user.
	 * 
	 * @param item
	 */
	void displayIncentive(IncentiveItem item);

}
