package org.threecixty.csincentive.core;

/**
 * interface implemented by the Incentive manager and registered in the CS
 * manager for callbacks.
 * 
 * 
 */
public interface IncentiveTriggerCallback {
	void newCrowdsourcingInfo(CSMetadata md);

}
