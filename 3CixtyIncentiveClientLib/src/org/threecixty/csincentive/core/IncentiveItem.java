package org.threecixty.csincentive.core;

public class IncentiveItem {
	String incentiveText;

	public IncentiveItem(String incentiveText) {
		this.incentiveText = incentiveText;
	}

	public String getIncentiveText() {
		return incentiveText;
	}

	public void setIncentiveText(String incentiveText) {
		this.incentiveText = incentiveText;
	}

}
