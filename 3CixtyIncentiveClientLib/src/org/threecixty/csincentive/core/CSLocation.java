package org.threecixty.csincentive.core;

public class CSLocation {

	AreaPos areaPos = null;
	GeoPos geoPos = null;
	AddressPos addrPos = null;
	TransportPos transportPos = null;

	public CSLocation() {

	}

	/**
	 * Computes if the location is within the range/area/transport/address of
	 * the provided loc. The provided location may be incomplete (e.g., only
	 * provide agency and line for the transport)
	 * 
	 * @param location
	 * @return true if the location is within the approximate location, false
	 *         otherwise
	 */
	public boolean withinLocation(CSLocation approxLocation) {

		XXX Saagar to implement
		return false;
	}

	//
	public GeoPos getGeoPos() {
		return geoPos;
	}

	public void setGeoPos(GeoPos geoPos) {
		this.geoPos = geoPos;
	}

	public GeoPos getGeoPos() {
		return geoPos;
	}

	public void setGeoPos(GeoPos geoPos) {
		this.geoPos = geoPos;
	}

	public AddressPos getAddrPos() {
		return addrPos;
	}

	public void setAddrPos(AddressPos addrPos) {
		this.addrPos = addrPos;
	}

	public TransportPos getTransportPos() {
		return transportPos;
	}

	public void setTransportPos(TransportPos transportPos) {
		this.transportPos = transportPos;
	}

	//
	public class GeoPos {
		long lat = 0;
		long lon = 0;
		int range = 0;
	}

	public class AreaPos {
		GeoPos ne = null;
		GeoPos sw = null;
	}

	public class AddressPos {
		String continent = null;
		String country = null;
		String state = null;
		String city = null;
		String area = null;
		String neighborhood = null;
		String street = null;
	}

	public class TransportPos {
		String agency = null;
		String line = null;
		String stop = null;
	}

}
