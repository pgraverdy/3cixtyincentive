package org.threecixty.csincentive.core;

public class FactIncentiveItem extends IncentiveItem {
	CSLocation location;

	public FactIncentiveItem(CSLocation location, String factText) {
		super(factText);

		this.location = location;
	}

}
